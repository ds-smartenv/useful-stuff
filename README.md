# Useful?
In this document we will provide some (possibly) useful urls. No garrantee that any of these URL's work, but they look interesting! 

## Social media related 
- Mastodon Dutch server [link](https://mastodon.nl/explore)
- Instagram scrapers [link](https://github.com/topics/instagram-scraper)
- Scrape google reviews? [link](https://github.com/omkarcloud/google-maps-scraper)
- Other google review scraper[link](https://outscraper.com/scraping-google-reviews-in-python/)
- A twitter scraper seems to work? [link](https://github.com/Altimis/Scweet) 

## Weather related 
- WOW data, open source, crowdsourced, weather data based on small weather stations.  [link](https://wageningenur4-my.sharepoint.com/:u:/g/personal/arno_timmer_wur_nl/EStS5B9dIC5NlSA2xxoa_WwBI6ARf426L5B4NsFvexZ2jA?e=edJvcU) - see also https://wow.knmi.nl/
