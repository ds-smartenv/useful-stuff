## Assorted API snippets

### Real-world considerations for Requests

Things do not always go so nicely, particularly when using API's at scale. We'll quickly cover some other common considerations when using API's, and outline how they can be solved.

#### Retries

Sometimes you can do everything perfectly, and send off a request but something on the web-server (or elsewhere) can go wrong and give a bad status code. We don't want to silently ignore these errors or let them crash our program by raising an exception. One common reason for the request to fail is that server is temporary unavailbable, or because you made a maximum amount of calls within a short amount of time (say 100 per second or something). Sometimes in this case the server might tell you that you did too many requests, and you should wait a little. A simple solution could be to simply retry the request again after a short amount of time. 

A hacky way to do this would be (**don't do this**):

``` python
import time
import requests

def get(url):
    try:
        # Try the request
        r = requests.get(url)
        
        # If the request has a bad status this fails 
        r.raise_for_status()  # raise an error on a bad status
    
    # everything below except will happen only if the try part failed
    except:
        # This makes the code wait for 1 second
        time.sleep(1)  # sleep for a bit in case that helps
        
        # Than it tries to try again
        return get(url)  # try again
```

An even better (but a little more complicated) and easier way to do this is to use the [tenacity](https://github.com/jd/tenacity) library:

``` python
import requests
from tenacity import (retry, stop_after_attempt, wait_fixed,
                      retry_if_exception_type)

@retry(stop=stop_after_attempt(3), wait=wait_fixed(0.1),
      retry=retry_if_exception_type(requests.HTTPError))
def get(url):
    try:
        r = requests.get(url)
        r.raise_for_status()  # raise an error on a bad status
        return r
    except requests.HTTPError:
        print(r.status_code, r.reason)
        raise
```

It uses a python decorator (the `@` symbol) to wrap our function with another function `retry` that will retry if we raise an error. We can tell it how many times to stop trying after, how long to wait between each retry, what error to retry on etc.

#### Authentication

Not all API's are open for immediate use. For example, some require you pay for access (e.g. the google maps API) and some require you to register for access first.

When you get access to a "closed" API, you will typically get an API key - a long string of letters and numbers - which is unique to you which you need to send along with any GET request you make to the API.
This lets the API know who you are and decide how to deal with your request.

Several different types of authentication exists (read the specific API docs) but the most common way is:
``` python
api_key = 'asodifhafglkkhj'
r = requests.get(url, auth=(api_key, ''))
```

#### Rate limits

API's can be costly to host and typically limit the number of requests that can be made (either by an IP or API key).
If you exceed this limit you'll get a `429` status code for any extra requests you make (and may be blocked if you continue making them).

It is important to therefore respect any rate limits given in an API's documentation (annoyingly some are very vague).
The simplest way to do this is to limit how often the number of times our function that makes the request can be called within some time limit using the [ratelim](https://pypi.org/project/ratelim/) library - again using decorators.

Using `ratelim.patient` is more kind to the API you are using as you lower the risk of overwhelming the API and getting a batch of responses with bad statuses.

```python
## Template

# Make the request
import requests

url = ...

response = requests.get(url)
response.raise_for_status()  # Gives an error if not successful

# Parse into JSON
payload = ...

# Define the index from `payload['dimension']`
index = ...

# Get data values
values = ...

# Read into dataframe, manipulate and plot
(pd.DataFrame(values, index=index)
    # Make dataframe wider (~1 line(s))
    # Get totals (1 line)
    # Plot (1 line)
)
```



```python
## COMPANIES HOUSE (UK registrar of companies)
# Register for an api key here: https://developer.companieshouse.gov.uk/developer/signin

api_key = 'put your api key here'
url = 'https://api.companieshouse.gov.uk/search?q=consultio consultius'
response = requests.get(url, auth=(api_key, ''))
response.raise_for_status()

response.json()['items'][0]  # get the first item
```

```python
## REDDIT - https://www.reddit.com/dev/api/#GET_subreddits_search
# Let's find some subreddits to learn python with!
# https://www.reddit.com/dev/api/#GET_subreddits_search

url = 'https://www.reddit.com/subreddits/search.json?q="learn python"&limit=5'
response = requests.get(url, headers={'User-agent': 'your bot 0.1'})
response.raise_for_status()

[result['data']['display_name_prefixed']
 for result in response.json()['data']['children']]
```

### List of API's

Massive list [**here**](https://github.com/public-apis/public-apis)


### API wrapper libraries

Massive list [**here**](https://github.com/realpython/list-of-python-api-wrappers).

For example, geo-code location data with [`geopy`](https://github.com/geopy/geopy) :

```python
from geopy.geocoders import Nominatim
geolocator = Nominatim(user_agent="ysi_tutorial")

print(geolocator.geocode("Leuvenlaan 4"))  # From address
print(geolocator.reverse("52.086779,5.1674726"))  # From co-ordinates
```
